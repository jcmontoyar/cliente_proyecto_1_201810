package model.logic;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.LinkedList;
import model.vo.VOCompania;
import model.vo.VOCompaniaServicios;
import model.vo.VOCompaniaTaxi;
import model.vo.VOInfoTaxiRango;
import model.vo.VORangoDistancia;
import model.vo.VORangoFechaHora;
import model.vo.VOServicio;
import model.vo.VOServicioResumen;
import model.vo.VOServiciosValorPagado;
import model.vo.VOTaxi;
import model.vo.VOZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override //1A
	public IQueue <VOServicio> darServiciosEnPeriodo(VORangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //2A
	public VOTaxi darTaxiConMasServiciosEnCompaniaYRango(VORangoFechaHora rango, String company)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //3A
	public VOInfoTaxiRango darInformacionTaxiEnRango(String id, VORangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //4A
	public LinkedList<VORangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //1B
	public LinkedList<VOCompania> darCompaniasTaxisInscritos() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //2B
	public VOTaxi darTaxiMayorFacturacion(VORangoFechaHora rango, String nomCompania) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //3B
	public VOServiciosValorPagado[] darServiciosZonaValorTotal(VORangoFechaHora rango, String idZona)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //4B
	public LinkedList<VOZonaServicios> darZonasServicios(VORangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //2C
	public LinkedList<VOCompaniaServicios> companiasMasServicios(VORangoFechaHora rango, int n)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //3C
	public LinkedList<VOCompaniaTaxi> taxisMasRentables()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //4C
	public VOServicioResumen darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	

}

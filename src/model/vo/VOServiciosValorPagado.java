package model.vo;

import model.data_structures.LinkedList;

public class VOServiciosValorPagado {
	
	private LinkedList<VOServicio> serviciosAsociados;
	private double valorAcumulado;
	public LinkedList<VOServicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(LinkedList<VOServicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public double getValorAcumulado() {
		return valorAcumulado;
	}
	public void setValorAcumulado(double valorAcumulado) {
		this.valorAcumulado = valorAcumulado;
	}
	
	

}

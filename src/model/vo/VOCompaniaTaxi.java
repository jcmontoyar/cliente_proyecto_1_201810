package model.vo;

public class VOCompaniaTaxi implements Comparable<VOCompaniaTaxi>{
	
	private String nomCompania;
	
	private VOTaxi taxi;

	public String getNomCompania() {
		return nomCompania;
	}

	public void setNomCompania(String nomCompania) {
		this.nomCompania = nomCompania;
	}

	public VOTaxi getTaxi() {
		return taxi;
	}

	public void setTaxi(VOTaxi taxi) {
		this.taxi = taxi;
	}

	@Override
	public int compareTo(VOCompaniaTaxi o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}

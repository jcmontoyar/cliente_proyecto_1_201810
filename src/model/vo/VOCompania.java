package model.vo;

import model.data_structures.LinkedList;

public class VOCompania implements Comparable<VOCompania> {
	
	private String nombre;
	
	private LinkedList<VOTaxi> taxisInscritos;	
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LinkedList<VOTaxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void setTaxisInscritos(LinkedList<VOTaxi> taxisInscritos) {
		this.taxisInscritos = taxisInscritos;
	}

	@Override
	public int compareTo(VOCompania o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	
	

}

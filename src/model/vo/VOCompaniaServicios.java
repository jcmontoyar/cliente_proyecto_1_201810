package model.vo;

import model.data_structures.LinkedList;

public class VOCompaniaServicios implements Comparable<VOCompaniaServicios> {
	
	private String nomCompania;
	
	private LinkedList<VOServicio> servicios;

	public String getNomCompania() {
		return nomCompania;
	}

	public void setNomCompania(String nomCompania) {
		this.nomCompania = nomCompania;
	}

	public LinkedList<VOServicio> getServicios() {
		return servicios;
	}

	public void setServicios(LinkedList<VOServicio> servicios) {
		this.servicios = servicios;
	}

	@Override
	public int compareTo(VOCompaniaServicios o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}

package model.vo;

import model.data_structures.LinkedList;

public class VOZonaServicios implements Comparable<VOZonaServicios>{

	private String idZona;
	
	private LinkedList<VOFechaServicios> fechasServicios;
	
	

	public String getIdZona() {
		return idZona;
	}



	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}



	public LinkedList<VOFechaServicios> getFechasServicios() {
		return fechasServicios;
	}



	public void setFechasServicios(LinkedList<VOFechaServicios> fechasServicios) {
		this.fechasServicios = fechasServicios;
	}



	@Override
	public int compareTo(VOZonaServicios o) {
		// TODO Auto-generated method stub
		return 0;
	}
}

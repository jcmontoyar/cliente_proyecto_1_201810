package model.vo;

import model.data_structures.LinkedList;

public class VOFechaServicios implements Comparable<VOFechaServicios>{
	
	private String fecha;
	private LinkedList<VOServicio> serviciosAsociados;
	private int numServicios;
	
	
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public LinkedList<VOServicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(LinkedList<VOServicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public int getNumServicios() {
		return numServicios;
	}
	public void setNumServicios(int numServicios) {
		this.numServicios = numServicios;
	}
	@Override
	public int compareTo(VOFechaServicios o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	

}

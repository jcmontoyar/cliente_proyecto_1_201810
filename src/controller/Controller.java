package controller;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.VOCompania;
import model.vo.VOCompaniaServicios;
import model.vo.VOCompaniaTaxi;
import model.vo.VOInfoTaxiRango;
import model.vo.VORangoDistancia;
import model.vo.VORangoFechaHora;
import model.vo.VOServicio;
import model.vo.VOServicioResumen;
import model.vo.VOServiciosValorPagado;
import model.vo.VOTaxi;
import model.vo.VOZonaServicios;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.cargarSistema(direccionJson);
	}
	//A1
	public static IQueue<VOServicio> darServiciosEnRango(VORangoFechaHora rango)
	{
		return manager.darServiciosEnPeriodo(rango);
	}

	//2A
	public static VOTaxi darTaxiConMasServiciosEnCompaniaYRango(VORangoFechaHora rango, String company)
	{
		return manager.darTaxiConMasServiciosEnCompaniaYRango(rango, company);
	}

	//3A
	public static VOInfoTaxiRango darInformacionTaxiEnRango(String id, VORangoFechaHora rango)
	{
		return manager.darInformacionTaxiEnRango(id, rango);
	}

	//4A
	public static LinkedList<VORangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		return manager.darListaRangosDistancia(fecha, horaInicial, horaFinal);
	}
	
	//1B
	public static LinkedList<VOCompania> darCompaniasTaxisInscritos()
	{
		return manager.darCompaniasTaxisInscritos();
	}
	
	//2B
	public static VOTaxi darTaxiMayorFacturacion(VORangoFechaHora rango, String nomCompania)
	{
		return manager.darTaxiMayorFacturacion(rango, nomCompania);
	}
	
	//3B
	public static VOServiciosValorPagado[] darServiciosZonaValorTotal(VORangoFechaHora rango, String idZona)
	{
		return manager.darServiciosZonaValorTotal(rango, idZona);
	}
	
	//4B
	public static LinkedList<VOZonaServicios> darZonasServicios (VORangoFechaHora rango)
	{
		return manager.darZonasServicios(rango);
	}
	
	//2C
	public static LinkedList<VOCompaniaServicios> companiasMasServicios(VORangoFechaHora rango, int n)
	{
		return manager.companiasMasServicios(rango, n);
	}

	//3C
	public static LinkedList<VOCompaniaTaxi> taxisMasRentables()
	{
		return manager.taxisMasRentables();
	}

	//4C
	public static VOServicioResumen darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha)
	{
		return manager.darServicioResumen(taxiId,horaInicial,horaFinal,fecha);
	}



}
